// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron');
const os = require('os');
const https = require('https');

const REPO_DOMAIN = 'gitlab.com';
const LIVE_DOMAIN = 'gitlab.io';
const PLATFORM = os.platform();

const local = process.env.NODE_ENV === 'local';
const remote = process.argv.includes('--remote');
const devTools = process.argv.includes('--dev-tools');

// get url from repository
function getRemoteURL(pkg) {
    // generate form repo url
    var url = pkg.repository;
    var repo = url.substr(url.indexOf(REPO_DOMAIN + '/') + REPO_DOMAIN.length + 1);
        repo = repo.substr(0, repo.indexOf('/'));
    var path = url.substr(url.indexOf(REPO_DOMAIN + '/') + REPO_DOMAIN.length + 1 + repo.length);
    return url.substr(0, url.indexOf(REPO_DOMAIN + '/')) + repo + '.' + LIVE_DOMAIN + path;
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
var pkg;

// add NODE_PATH to require paths
require.main.paths.unshift(process.env.NODE_PATH);

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
    try {
        if (local && !remote) {
            createWindow(require(process.cwd() + '/package.json'));
        }
        else {
            if (local) {
              pkg = require(process.cwd() + '/package.json');
            }
            else {
              pkg = require('./src/package.json');
            }

            var url = getRemoteURL(pkg) + '/package.json';
            https.get(url, res => {
              res.setEncoding('utf8');
              let body = '';
              res.on('data', data => {
                  body += data;
              });
              res.on('end', () => {
                var embedded = res.statusCode !== 200;
                  if (!embedded) {
                      pkg = JSON.parse(body);
                  }
                  else {
                    console.error('status code for "' + url + '": ' + res.statusCode);
                  }
                  createWindow(pkg, embedded);
              });
          });
        }
    }
    catch(e) {
        if (local && e.code === 'MODULE_NOT_FOUND') {
            process.exit();
        }
        throw e;
    }
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  //if (process.platform !== 'darwin') {
    app.quit();
  //}
});

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

function createWindow (pkg, embedded) {
  // Create the browser window.
  if (!pkg.electron) {
    pkg.electron = {};
  }
  if (pkg.ui) {
    pkg.electron.width = pkg.ui.width;
    pkg.electron.height = pkg.ui.height;
    pkg.electron.resizable = pkg.ui.resizable;
  }
  if (!pkg.electron.show) {
    pkg.electron.show = false; 
  }
  if (PLATFORM == 'win32' && !pkg.electron.frame) {
    pkg.electron.frame = false;
  }
  if (PLATFORM == 'darwin' && !pkg.electron.titleBarStyle) {
    pkg.electron.titleBarStyle = 'hiddenInset';
  }
  mainWindow = new BrowserWindow(pkg.electron);

  if (local && !remote) {
    var StaticServer = require('static-server');
    var server = new StaticServer({
      rootPath: process.cwd(),
      port: 48294
    });
      
    server.start(function () {
      console.log('Started local development server on port', server.port);
      mainWindow.loadURL('http://localhost:' + server.port + '/index.html');
    });
  }
  else if (embedded && !remote) {
    // loading es6 modules will not work from file path :-(
      //TODO - in future version show built in 'app unavailable message'
    mainWindow.loadFile('./src/index.html');
  }
  else {
    // and load the index.html of the app.
    mainWindow.loadURL(getRemoteURL(pkg));
  }

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  // Show main window when it is ready (prevents white background flash)
  mainWindow.on('ready-to-show', function() {
    mainWindow.show();
    mainWindow.focus();

    if (devTools) {
      try {
        mainWindow.openDevTools();
      }
      catch(e) {
        console.error(e);
      }
    }
  });
}