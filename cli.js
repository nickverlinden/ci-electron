#!/usr/bin/env node

var path = require('path');
var proc = require('child_process');
var electron;
try {
    electron = require('electron');
}
catch(e) {
    if (e.code === 'MODULE_NOT_FOUND') {
        console.error('You need to install "ci-electron" in dev mode in order to use it for testing your app.\nDo this by performing this command: "npm install --dev ci-electron".');
        process.exit(1);
    }
    throw e;
}

if (process.argv[2] == 'run') {
    var cwd = path.join(process.cwd(), process.argv[3] || '');

    var env = Object.create( process.env );
    env.NODE_ENV = 'local';
    env.NODE_PATH = path.join(cwd, 'node_modules');

    var childArgs = process.argv.slice(4);
    childArgs.unshift(__dirname);

    var child = proc.spawn(electron, childArgs, { cwd: cwd, stdio: 'inherit', windowsHide: false, env: env });
    child.on('close', function (code) {
        process.exit(code);
    })

    const handleTerminationSignal = function (signal) {
    process.on(signal, function signalHandler () {
        if (!child.killed) {
        child.kill(signal);
        }
    })
    }

    handleTerminationSignal('SIGINT');
    handleTerminationSignal('SIGTERM');
}
else {
    console.error('ERR! unknown command "' + process.argv[2] + '".\n\nCOMMANDS:\n\trun <path> <options>\tStarts electron and loads the app in the given path. (default path is current directory)\n\t\t--remote\tLoads the version of the app hosten on gitlab.io.\n\t\t--dev-tools\tOpens chromium dev tools on start.');
}
