# GitLab CI recipe for building redistributable electron packages

# requirements

Add a package.json to the root of your web app. Make sure the name, version and author fields are provided. The productName field will also be used as the app name (example: name could be 'youtube-to-wav' and productName could be 'YouTube To WAV').

There is also support for providing electron specific settings in the 'electron' property of the package.json. These properties are directly used for creating a BrowserWindow in electron, check the BrowserWindow documentation for a list of all options.

If a 'ui' property is present in the package.json, it will take precedence over the electron property. Supported values are:
```json
{
    "ui" : {
        "width" : 800,
        "height" : 600,
        "resizable" : false
    }
}
```

# using for development

Install the package from npm using dev dependencies like this:
'npm install -g --dev ci-electron'

After that, you can launch your app from a terminal by executing the command below in the root directory of the project.
'ci-electron run ./'

It will run your web app as if it was running from the automated builds (except for the app icon and the App name, it will be the default electron icon and 'electron' app name).

# automating cross-platform build

All you need to do is host your project on gitlab.com, add a valid package.json file to the root of your project. Then add a file called '.gitlab-ci.yml' to the root of your project.

The automated build will always use the latest version hosted on GitLab pages, so you don't need users to install a new build (unless you need a newer version of electron).

In the example gitlab-ci.yml file below, the app packages are created in a subfolder called 'packages' of your project, and the app is hosted on GitLab Pages.
The filenames in the packages folder are as follows: '<name>.exe' for Windows, '<name>.zip' for macOS, '<name>.tar.gz' for linux. If a productName was specified in the package.json, it will be used instead of the name (example: name could be 'youtube-to-wav' and productName could be 'YouTube To WAV', then the productName 'YouTube To WAV' is used as the filename of the build).
You can provide download links to these files in your app. They will be available when the app is deployed to GitLab Pages.

```yml
stages:
  - pack
  - deploy

####################
# PACK
####################

pack:
  stage: pack
  only:
    - tags
  image: electronuserland/electron-builder:wine
  before_script:
    - mkdir .ci-electron
    - git clone https://gitlab.com/nickverlinden/ci-electron .ci-electron
    - mkdir .ci-electron/src
    - cp -r * .ci-electron/src
    - cd .ci-electron
    - npm -q install
  script:
    - npm run prepack
    - npm run pack
    - rm -rf packages/mac
    - rm -rf packages/win-unpacked
    - rm -rf packages/linux-unpacked
    - mv packages ../packages
  artifacts:
    paths:
      - packages
    expire_in: 1h


####################
# DEPLOY
####################

pages:
  stage: deploy
  only:
    - tags
  dependencies: 
    - pack
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
```